require 'csv'

Dir['../lib/*.rb'].each { |file| require_relative(file) }

arguments = ArgumentsParser.new
arguments.check

CSV.open("#{arguments.filename}.csv", 'wb', encoding: "utf-8") do |rows|

#   rows << ['Название', 'Цена', 'Изображение', 'Срок доставки', 'Код']
  site = SiteParser.new(arguments.site_link)
  site.get_product_links.each do |link|
    site.get_product_page(link)
    site.get_product_parameters.each { |product| rows << product }
  end
end


