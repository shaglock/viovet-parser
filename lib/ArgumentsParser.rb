require 'optparse'
require 'open-uri'

class ArgumentsParser
  attr_reader :site_link, :filename
  def initialize
    parser = OptionParser.new { |opts| opts.banner = 'Usage: ruby test.rb [category_link] [filename]' }
    parser.parse!
    @site_link = ARGV[0]
    @filename = ARGV[1]
  end

  def check_arguments_presence
    if ARGV.empty? || ARGV[1].nil?
      puts 'Usage: ./test.rb [category_link] [csv-file name]'
      exit 1
    end
  end

  def  check_arguments_correctness
    if  @site_link.scan(URI.regexp).empty?
      puts 'wrong link entered'
      exit 1
    elsif  @filename.match(/[^0-9A-Za-z.\-]/)
      puts 'wrong filename entered'
      exit 1
    end
  end

  def check
    check_arguments_presence
    check_arguments_correctness
    puts "Parsing #{@site_link} to #{@filename}.csv"
  end
end
