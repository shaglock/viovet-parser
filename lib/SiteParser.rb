require 'curb'
require 'nokogiri'

class SiteParser
  def initialize(site_link)
    @site_link = site_link
    @base_link = URI.parse(site_link).host
    @pages_count = Nokogiri::HTML(Curl.get(site_link).body_str).xpath("//div[@class='pagination']/ul/li[last()-1]/a").text.to_i
  end

  def get_product_links
    product_links_array = []
    @pages_count.times do |page_number|
      product_links = Nokogiri::HTML(Curl.get(@site_link + "?page=#{page_number + 1}").body_str).xpath("//*[@id='search_result_listings_with_footer_nav']/ul/li/a/@href")
      product_links.each { |link| product_links_array << link.text }
    end
    product_links_array
  end

  def get_product_page(link)
    @product_page = Nokogiri::HTML(Curl.get("https://" + @base_link + link).body_str)
    while @product_page.xpath("//*[@id='product_listing']").text.empty?
      @product_page = Nokogiri::HTML(Curl.get("https://" + @base_link+link).body_str)
    end
    @product_page
  end

  def check_errors
    if @product[:dispatch_time].empty?
      @product[:dispatch_time] = @product_page.xpath("//div[@class='col _1']/strong[@class='stock out-stock']/text()").text.strip
    end
    if @product.has_value?("https:")
      @product[:product_image] = "https:" + @product_page.xpath("//*[@id='bodyarea']/div/div/div/div/div[@class='gridbox one-quarter']/a/@href").text
    end
  end

  def get_product_parameters
    products_array = []
    title = @product_page.xpath("//*[@id='product_family_heading']").text
    @product_page.xpath("//*[@id='product_listing']/li[@class=' product clearfix ']").each do |item|
      @product = {
        product_name:        title + " - " + item.xpath("./div[@class='col _1']/div[@class='title']").text.strip,
        product_price:       item.xpath("./div[@class='col _2']/div[@class='ours']/span/span/text()").text,
        product_image:       "https:" + item.xpath("./div[@class='col _1']/ul/li/a[@class='somebutton photo-view fancybox-thumb']/@href").text,
        dispatch_time:       item.xpath("./div[@class='col _1']/strong[@class='stock in-stock']/text()").text.strip,
        product_code:        item.xpath("./div[@class='col _1']/ul/li/a[@class='somebutton item-code nolink']/strong/text()").text
        }
      check_errors
      products_array << @product.values
    end
    products_array
  end
end